<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PreferredTypeController;
use App\Http\Controllers\UserPreferredTypeController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\SpaceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register',[UserController::class,'register']);
Route::post('login',[UserController::class,'login']);
Route::put('user/edit',[UserController::class,'edit']);
Route::post('preferredtype/add',[PreferredTypeController::class,'add']);
Route::get('preferredtype/get',[PreferredTypeController::class,'get']);
Route::post('userpreferredtype/add',[UserPreferredTypeController::class,'add']);
Route::get('userpreferredtype/getbyuserid/{id}',[UserPreferredTypeController::class,'getPreferredTypeByUserId']);
Route::delete('userpreferredtype/delete/{id}',[UserPreferredTypeController::class,'delete']);
Route::post('file/{id}',[FileController::class,'file']);
Route::get('space/get',[SpaceController::class,'getAll']);
Route::post('space/add',[SpaceController::class,'add']);
Route::get('space/get/{id}',[SpaceController::class,'getById']);
Route::put('space/edit',[SpaceController::class,'edit']);
Route::delete('space/delete/{id}',[SpaceController::class,'delete']);