<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;

class FileController extends Controller
{
    public function file(Request $request, $id) {
        $file = new File;
        if($request->hasFile('file')){
            $completeFileName = $request->file('file')->getClientOriginalName();
            $fileNameOnly = pathinfo($completeFileName, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $compPic = str_replace(' ', '_', $fileNameOnly.'-'.rand() . '_'.time(). '.'.$extension);
            $path = $request->file('file')->storeAs('public/files', $compPic);
            $file->file = $compPic;
            $file->user_id = $id;
        }
        if($file->save()){
            return ['status' => true, 'message' => 'Post Saved Successfully'];
        } else {
            return ['status' => false, 'message' => 'Something went wrong!'];
        }
    }
}
