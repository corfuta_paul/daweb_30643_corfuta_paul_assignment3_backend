<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserPreferredType;

class UserPreferredTypeController extends Controller
{
    function add(Request $req)
    {
        $pref1 = UserPreferredType::where('user_id', '=', $req->user_id)->where('preferred_type_id', '=', $req->preferred_type_id)->first();

        if ($pref1 === null) {
            $pref = new UserPreferredType;
            $pref->user_id= $req->input('user_id');
            $pref->preferred_type_id= $req->input('preferred_type_id');
            $pref->save();
            return $pref;      
        }

        return "This record already exists";
    }

    function getPreferredTypeByUserId($id)
    {
        $pref = UserPreferredType::where('user_id', '=', $id)->get();
        return $pref;
    }

    function delete($id)
    {
        $pref = UserPreferredType::find($id);
        $pref->delete();
    }
}
