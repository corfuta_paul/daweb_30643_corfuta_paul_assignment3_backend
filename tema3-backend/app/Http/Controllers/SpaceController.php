<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Space;

class SpaceController extends Controller
{
    function getAll()
    {
        $space = Space::all();
        return $space;
    }

    function getById($id)
    {
        $space = Space::where('id',$id)->first();
        return $space;
    }

    function add(Request $req)
    {
        $space = new Space;
        $space->name= $req->input('name');
        $space->dimension= $req->input('dimension');
        $space->city= $req->input('city');
        $space->price= $req->input('price');
        $space->photo= $req->input('photo');
        $space->description= $req->input('description');
        $space->action_type= $req->input('action_type');
        $space->space_type= $req->input('space_type');
        $space->save();
        return $space->id;
    }

    function edit(Request $req)
    {
        $space= Space::where('id', '=', $req->id)->first();
        $space->update(['name' => $req->name]);
        $space->update(['dimension' => $req->dimension]);
        $space->update(['city' => $req->city]);
        $space->update(['price' => $req->price]);
        $space->update(['photo' => $req->photo]);
        $space->update(['description' => $req->description]);
        $space->update(['action_type' => $req->action_type]);
        $space->update(['space_type' => $req->space_type]);
        return $space;
    }

    function delete($id)
    {
        $space = Space::find($id);
        $space->delete();
    }
}
