<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PreferredType;

class PreferredTypeController extends Controller
{
    function add(Request $req)
    {
        $pref = new PreferredType;
        $pref->name= $req->input('name');
        $pref->save();
        return $pref;
    }

    function get()
    {
        $pref = PreferredType::all();
        return $pref;
    }
}
